﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class HighScoreUI : MonoBehaviour {

    Text HighScoreText;


	void Start ()
    {
        HighScoreText = GetComponent<Text>();
	}
	

	void Update ()
    {
        HighScoreText.text = "HighScore:" + GameControl.instance.HighScore;
	}
}
