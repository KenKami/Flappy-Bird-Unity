﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;

public class SwitchScene : MonoBehaviour {

    public string SceneTarget;

    public void Switchscene()
    {
        SceneManager.LoadScene(SceneTarget);
    }


}
