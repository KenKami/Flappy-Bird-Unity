﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEngine;

public class GameControl : MonoBehaviour {

    public static GameControl instance;
    public GameObject gameOverText;
    public bool gameOver = false;
    public float scrollSpeed = -1.5f;
    public Text scoreText;
    public GameObject Music;
    public bool Musique = true;
    public GameObject GameoverMusic;
    public bool GameOvermusic = false;

    public int HighScore { get; protected set; }
    private int score = 0;

    private int Score
    {
        get
        {
            return score;
        }

        set
        {
            if (value != score)
            {
                score = value;
                
                if (score > HighScore)
                {
                    HighScore = score;
                    PlayerPrefs.SetInt("HighScore", HighScore);
                }
            }
            
        }
    }

	void Awake ()
    {
		if (instance == null)
        {
            instance = this;
        }
        else if (instance != this)
        {
            Destroy(gameObject);
        }

        HighScore = PlayerPrefs.GetInt("HighScore", 0);
	}
	
	void Update ()
    {
		if (gameOver == true && Input.GetMouseButtonDown(0))
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        }
	}

    public void BirdScored()
    {
        if (gameOver)
        {
            return;
        }

        Score++;
        scoreText.text = "Score: " + Score.ToString();
    }



    public void BirdDied()
    {
        gameOverText.SetActive(true);
        gameOver = true;
        Music.SetActive(false);
        Musique = false;
        GameoverMusic.SetActive(true);
        GameOvermusic = true;
      
    }
}
