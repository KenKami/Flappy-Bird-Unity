﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Playlist_Spatial : MonoBehaviour {

    Object[] myMusic;

    private void Awake()
    {
        myMusic = Resources.LoadAll("Music", typeof(AudioClip));
        GetComponent<AudioSource>().clip = myMusic[1] as AudioClip;
    }

    // Use this for initialization
    void Start () {
        GetComponent<AudioSource>().Play();
	}
	
	
}
